from socket import *
from sys import *
from threading import *
from methods.get import get_method
from methods.delete import delete_method
from methods.post import post_method
from methods.put import put_method
import time
from config import connections

def receive_response(req, req_method):
    if(req_method == "GET"):
        return get_method(req, req_method)
    elif(req_method == "HEAD"):
        return get_method(req, req_method)
    elif(req_method == "POST"):
        return post_method(req)
    elif(req_method == "DELETE"):
        return delete_method(req)
    elif(req_method == "PUT"):
        return put_method(req)
    

def get_client(client_socket, client_address):
    client_socket.settimeout(10)
    while True:
        try:
            try: 
                req = client_socket.recv(10485760)
                if (not req):
                    break
                req = req.decode('utf-8')
                req_method = req.split('\n')[0].split(' ')[0]
                response = receive_response(req, req_method)
                try:
                    client_socket.send(response[0].encode('utf-8'))
                except TypeError:
                    pass
                try:
                    if (response[1] != None):
                        client_socket.send(response[1])
                except TypeError:
                    pass
            except ConnectionResetError:
                pass
        except timeout:
            client_socket.close()
            break
    return

port = int(argv[1])

server_socket = socket(AF_INET, SOCK_STREAM)
server_socket.bind(('', port))
server_socket.listen(5)
print("Listening on port {}".format(port))
while True:
    client_socket, client_address = server_socket.accept()
    new_thread =  Thread(target = get_client, args = (client_socket, client_address))
    new_thread.start()
    if active_count() > connections:
        print('Too many connections! Try again later!')
        time.sleep(10)
    