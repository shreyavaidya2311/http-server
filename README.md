<div align="center">
<img width=350px src="https://user-images.githubusercontent.com/56782318/142733042-1983f78f-8f88-457f-9819-c01f8f0e1ec0.png" alt="logo"/>

![Status](https://img.shields.io/badge/status-active-success.svg)
</div>


## About HTTP
The **Hypertext Transfer Protocol** (HTTP) is an application-level protocol for distributed, collaborative, hypermedia information systems. It is a generic, stateless, protocol which can be used for many tasks beyond its use for hypertext, such as name servers and distributed object management systems, through extension of its request methods, error codes and headers. A feature of HTTP is the typing and negotiation of data representation, allowing systems to be built independently of the data being transferred.

## About the Project

**HTTP Server** is a multithreaded web server built using **Python3** based on the **HTTP/1.1 protocol**. It supports and services `GET`, `POST`, `HEAD`, `PUT` and `DELETE` requests.

## Installation & Usage

**1. Clone the repository**
```
git clone https://gitlab.com/shreyavaidya2311/http-server.git
```

**2. Run the server**
```
python3 server.py [PORT_NUMBER]
```

**3. For automated testing**
```
python3 server.py [OPTIONS] [PORT_NUMBER]
```
#### _Options for Testing_

- -g : Tests `GET` requests
- -h: Tests `HEAD` requests
- -d : Tests `DELETE` requests
- -po: Tests `POST` requests
- -pu : Tests `PUT` requests

_When no option is chosen, all requests are tested._

## References

- <a href="https://datatracker.ietf.org/doc/html/rfc2616"> RFC 2616 </a>
- <a href="https://developer.mozilla.org/en-US/docs/Web/HTTP"> MDN </a>

