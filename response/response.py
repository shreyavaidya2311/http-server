from response.statuscodes import status_codes
import pytz
import datetime

def get_response(headers, code, file_content):
    if (code not in status_codes.keys()):
        return
    date = datetime.datetime.now(tz = pytz.utc)
    time = " {}:{}:{} GMT".format(date.strftime("%H"),date.strftime("%M"),date.strftime("%S"))
    date = date.strftime("%a") + ', ' + str(date.strftime("%d")) + " " + date.strftime("%b") + " " + str(date.year) + time 
    response = "HTTP/1.1 {} {}\r\n".format(code, status_codes[code])
    for k, v in headers.items():
        if v is not None:
            response += "{}: {}\r\n".format(k, v)
    response += "Date: {}\r\n\r\n".format(date)
    
    if(file_content == None):
        file_content = b'<html><head><h1>Record Saved</h1></head></html>'
    res = [response, file_content]
    return res