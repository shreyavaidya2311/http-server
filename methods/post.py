import random
from response.responseHeaders import response_headers
from response.response import get_response
import os
import string
from config import post_dump

def post_method(req):
    headers = []
    code = None
    for i in req.split('\n'):
        headers.append(i)
    headers_list = {}
    for i in headers[1:]:
        try:
            headers_list[i.split(':')[0]] = i.split(': ')[1]
        except IndexError:
            pass
    
    res = dict(response_headers)
    id = "id_" + str(random.randint(1000, 350000)) + random.choice(string.ascii_letters) + ".txt"
    location = post_dump + "/" + id

    file_path = headers[0].split(' ')[1]
    file_path = post_dump + file_path
    
    if os.path.exists(file_path):
        try:
            file = open(file_path, "w+") 
            code = 204
        except:
            return get_response(res, 403, None)
    else:
        file = open(location, "w+") 
        code = 201

    res["Accept-Ranges"] = "bytes"

    if("Cookie" in headers_list):
        cookie = headers_list["Cookie"]
    else:
        set_cookie = str(random.randint(10000,50000))
        res["Set-Cookie"] = set_cookie

    try:
        connection = headers_list["Connection"]
        if(connection.strip() == "keep-alive"):
            res["Keep-Alive"] = True
    except:
        res["Keep-Alive"] = False

    res["Content-Type"] = headers_list["Content-Type"]
    if(headers_list["Content-Type"].strip() == "application/x-www-form-urlencoded"):

        values = ''
        for header in headers[1:]:
            if ':' in header:
                pass
            else:
                if header != '\r' and header != '\n':
                    values = header

        post_values = values.split("&")
        fields = {}
        for val in post_values:
            valid_value = val.split("=")
            if(len(valid_value) == 2):
                fields[str(valid_value[0])] = valid_value[1]
            else:
                pass
        file.write(str(fields))
        file.close()
        res["Content-Location"] = location
        return get_response(res, code, None)

    elif(headers_list["Content-Type"].split("; ")[0] == "multipart/form-data"):
        boundary = headers_list["Content-Type"].split("; ")[1]
        boundary = "--" + boundary.split("=")[1]
        values = []
        for i in req.split('\n'):
            values.append(i)
        set_boundary = 0
        raw_data = []
        for i in values:
            if(i == boundary):
                set_boundary = 1
                raw_data.append(i)
            elif(set_boundary == 1):
                raw_data.append(i)
            else:
                pass
        form_values =  []
        for i in raw_data:
            if(i == boundary or i == '\r' or i == ''):
                pass
            else:
                form_values.append(i)
        
        form_values.pop()
        form_data = {}
        val = None
        for i in form_values:
            try:
                val = i.split("name=")[1]
                val = val.split('\r')[0]
                val = val[1:-1]
                form_data[val] = None
            except IndexError:
                data = i.split('\r')[0]
                form_data[val] = data
        
        file = open(location, "w+") 
        file.write(str(form_data))
        file.close()
        res["Content-Location"] = location
        return get_response(res, code, None)
    else:
        return get_response(res, 415, None)
