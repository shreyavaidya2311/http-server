from response.responseHeaders import response_headers
import os
from response.response import get_response
from config import post_dump
import random


def delete_method(req):
    headers = []
    for i in req.split('\n'):
        headers.append(i)
    headers_list = {}
    for i in headers[1:]:
        try:
            headers_list[i.split(':')[0]] = i.split(': ')[1]
        except IndexError:
            pass
    
    res = dict(response_headers)

    if("Cookie" in headers_list):
        cookie = headers_list["Cookie"]
    else:
        set_cookie = str(random.randint(10000,50000))
        res["Set-Cookie"] = set_cookie

    res["Accept-Ranges"] = "bytes"

    try:
        connection = headers_list["Connection"]
        if(connection.strip() == "keep-alive"):
            res["Keep-Alive"] = True
    except:
        res["Keep-Alive"] = False

    file_path = headers[0].split(' ')[1] 
    file_path = post_dump + file_path

    res["Content-Location"] = file_path

    if os.path.exists(file_path):
        if os.access(file_path, os.W_OK):
            os.remove(file_path)
            return get_response(res, 200, None)
        else:
            return get_response(res, 403, None)

    else:
        return get_response(res, 404, None)