from response.responseHeaders import response_headers
from response.response import get_response
import os
from config import post_dump
import random

def put_method(req):
    headers = []
    code = None
    for i in req.split('\n'):
        headers.append(i)
    headers_list = {}
    for i in headers[1:]:
        try:
            headers_list[i.split(':')[0]] = i.split(': ')[1]
        except IndexError:
            pass

    file_path = headers[0].split(' ')[1]
    file_path = post_dump + file_path

    res = dict(response_headers)

    if("Cookie" in headers_list):
        cookie = headers_list["Cookie"]
    else:
        set_cookie = str(random.randint(10000,50000))
        res["Set-Cookie"] = set_cookie

    res["Accept-Ranges"] = "bytes"

    try:
        connection = headers_list["Connection"]
        if(connection.strip() == "keep-alive"):
            res["Keep-Alive"] = True
    except:
        res["Keep-Alive"] = False

    if os.path.exists(file_path):
        if os.access(file_path, os.W_OK):
            code = 204
        else:
            code = 403
            get_response(res, 403, None)
    else:
        code = 201

    values = ''
    for header in headers[1:]:
        if ':' in header:
            pass
        else:
            if header != '\r' and header != '\n':
                values = header

    post_values = values.split("&")
    fields = {}
    for val in post_values:
        valid_value = val.split("=")
        if(len(valid_value) == 2):
            fields[str(valid_value[0])] = valid_value[1]
        else:
            pass
    res["Content-Type"] = headers_list["Content-Type"]
    file = open(file_path, 'w+')
    file.write(str(fields))
    file.close()
    res["Content-Location"] = file_path
    return get_response(res, code, None)