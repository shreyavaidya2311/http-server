import gzip
import zlib
import brotli
import os
import random
from response.responseHeaders import response_headers
from response.response import get_response
from config import root
import time

def get_method(req, req_method):
    headers = []
    for i in req.split('\n'):
        headers.append(i)
    headers_list = {}
    for i in headers[1:]:
        try:
            headers_list[i.split(':')[0]] = i.split(': ')[1]
        except IndexError:
            pass
    res = dict(response_headers)      
    acceptable_format = headers_list['Accept']
    accept = acceptable_format.split(',')
    accept = accept[:-1]
    acceptable_encoding = None

    try:
        acceptable_encoding = headers_list['Accept-Encoding'].split(',')
    except:
        acceptable_encoding = None

    file_extensions = {
    ".bin": "application/octet-stream",
    ".bz": "application/x-bzip",
    ".css": "text/css",
    ".gz": "application/gzip",
    ".gif": "image/gif",
    ".html": "text/html",
    ".ico": "image/avif",
    ".jpg": "image/jpeg",
    ".min": "application/javascript",
    ".png": "image/avif",
    ".js": "application/javascript",
    ".json": "application/json",
    ".mp3": "audio/mpeg",
    ".ogg": "video/ogg",
    ".php": "application/x-httpd-php",
    ".pdf": "application/pdf",
    ".txt": "text/html",
    ".webm": "video/webm",
    }
    file_path = headers[0].split(' ')[1]
    
    if(file_path == "/"):
        file_path = root + "/index.html"
    else:
        file_path = root + file_path
    res["Content-Location"] = file_path

    if('.' in file_path):
        file_ext = '.' + file_path.split('.')[1]
        try:
            ext = file_extensions[file_ext]
        except KeyError:
            return get_response(res, 415, None)
        if(file_extensions[file_ext] in accept or len(accept) == 0):
            content_type = file_extensions[file_ext]
            res["Content-Type"] = content_type
        else:
            return get_response(res, 406, None)

    file = None
    try:
        file = open(file_path, "rb")
    except FileNotFoundError:
        return get_response(res, 404, None)

    file_content = file.read()
    content_length = len(file_content)

    if(acceptable_encoding == None):
        pass

    elif(acceptable_encoding[0] == "gzip"):
        file_content = gzip.compress(file_content)
        content_length = len(file_content)
        
    elif(acceptable_encoding[0] == "deflate"):
        file_content = zlib.compress(file_content)
        content_length = len(file_content)
        
    elif(acceptable_encoding[0] == "br"):
        file_content = brotli.compress(file_content)
        content_length = len(file_content)
        
    if(acceptable_encoding != None):
        res["Content-Encoding"] = acceptable_encoding[0]
    res["Content-Length"] = content_length

    lastModified = time.ctime(os.path.getmtime(file_path))
    res["Last-Modified"] = lastModified

    if("Cookie" in headers_list):
        cookie = headers_list["Cookie"]
    else:
        set_cookie = str(random.randint(10000,50000))
        res["Set-Cookie"] = set_cookie

    try:
        connection = headers_list["Connection"]
        if(connection.strip() == "keep-alive"):
            res["Keep-Alive"] = True
    except:
        res["Keep-Alive"] = False
    
    if(req_method == "HEAD"):
        return get_response(res, 200, None)      
    return get_response(res, 200, file_content)