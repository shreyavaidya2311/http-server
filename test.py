from socket import *
from threading import Thread
import requests
import sys
import os
from config import root, post_dump
from response.statuscodes import status_codes
from colorama import Fore, Style

def get_status_code(res):
    code = (str(res).split("[")[1]).split("]")[0]
    return int(code)

def print_headers(res):
    for i in res.headers:
        print(i + ": " + res.headers[i])

def get(path):
    res = requests.get(path)
    code = get_status_code(res)
    print(Fore.GREEN + "GET {} {}".format(code, status_codes[code]))
    print(Style.RESET_ALL)
    print_headers(res)
    print()

def head(path):
    res = requests.head(path)
    code = get_status_code(res)
    print(Fore.GREEN + "HEAD {} {}".format(code, status_codes[code]))
    print(Style.RESET_ALL)
    print_headers(res)
    print()

def post(path, obj):
    res = requests.post(path, data=obj)
    code = get_status_code(res)
    print(Fore.GREEN + "POST {} {}".format(code, status_codes[code]))
    print(Style.RESET_ALL)
    print_headers(res)
    print()

def delete(path):
    res = requests.delete(path)
    code = get_status_code(res)
    print(Fore.GREEN + "DELETE {} {}".format(code, status_codes[code]))
    print(Style.RESET_ALL)
    print_headers(res)
    print()

def put(path,obj):
    res = requests.put(path, data = obj) 
    code = get_status_code(res)
    print(Fore.GREEN + "PUT: {} {}".format(code, status_codes[code]))
    print(Style.RESET_ALL)
    print_headers(res)
    print()

port = None
flag = None

if(len(sys.argv) == 2):
    port = sys.argv[1]

elif(len(sys.argv) == 3):
    port = sys.argv[2]
    flag = sys.argv[1]

host = '127.0.0.1'
doc = "http://" + 	host + ":" + port 

files = os.listdir(root)

paths = []
for file in files:
    paths.append(doc + '/' + file)
paths.append(doc + "/doesnotexist.txt")
paths.append(doc + "/wrong.png")

post_files = os.listdir(post_dump)
post_paths = []
for post_file in post_files:
    post_paths.append(doc + '/' + post_file)

if(flag == "-g" or flag == None):
    for path in paths:
        Thread(target = get, args=(path,)).start()


if(flag == "-h" or flag == None):
    for path in paths:
        Thread(target = head, args=(path,)).start()


if(flag == "-d" or flag == None):
    for post_path in post_paths:
        Thread(target = delete, args=(post_path,)).start()
    for path in paths:
        Thread(target = delete, args=(path,)).start()

if(flag == "-po" or flag == None):
    path = doc + "/post-data"
    test_data = {
    "fruit": "Apple",
    "size": "Large",
    "color": "Red"
    }
    Thread(target = post, args=(path,test_data,)).start()
    for post_path in post_paths:
        Thread(target = post, args=(post_path,test_data,)).start()

if(flag == "-pu" or flag == None):
    test_data = {
    "fruit": "Orange",
    "size": "Large",
    "color": "Orange"
    }
    path = doc + "/id_1795D.txt"
    Thread(target = put, args=(path,test_data,)).start()
    for post_path in post_paths:
        Thread(target = post, args=(post_path,test_data,)).start()